﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using TMPro;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using UnityEngine.Events;

public class DialogueSystem : MonoBehaviour
{

    public static DialogueSystem instance = null;
    public UnityEvent OnDialogueComplete;
    //Bust image
    public Image[] characterImage;

    //Option bool
    public bool skipTextActive = false; //used to show the entire text than waiting
    public float textSpeed = 0.2f;
    public bool isDialCompleted = false;

    //devcontrol

    //textmanagement
    public DialogueSet activeDialogue;
    [SerializeField]
    private string activeDialogueText;

    //public DialogueSet dialogueSets;
    public TextMeshProUGUI displayText;
    public TextMeshProUGUI characterTextName;

    public int index;
    public int dialogueTotal;
    //Dialogue Log
    //DialogueLog dialogueLog;

    //dialogue graphics
    //public Image continueDialogueIcon;
    //public Image dialogueBox;
    public Image targetImage;
    //Dev Settings
    public float boxWidth;
    public float boxHeight;

    //public GameObject pressbuttonObj;
    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(this.gameObject);
    }

    // Start is called before the first frame update
    void Start()
    {
        //initial erase
        EraseDialogue();
        this.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("space")) //insert input here!
        {

            Debug.Log("active dialogue ID:" + index);
            StopAllCoroutines();
            /* pierre:
             if there is a Dialogue running, show the rest of the Dialogue (like skip)
                but dont skip the entire Dialogue
             else start printing the next Dialogue
             */
            //pressbuttonObj.SetActive(false);
            ContCoverstation();
        }

        //check if its the last dialogue
        if (index > dialogueTotal)
        {
            //call event
            this.gameObject.SetActive(false);
            //pressbuttonObj.SetActive(true);
            OnDialogueComplete.Invoke();
            //Debug.Log("dialogue Completed");
        }
    }


    //use this to call in a certain dialogue
    public void activateDialogue(DialogueSet dialogueSetID)
    {
        isDialCompleted = false;
        index = 0; //sets index back to zero
        activeDialogue = dialogueSetID; //sets the dualogue
        dialogueTotal = activeDialogue.getTotalDialogue();
        Debug.Log(dialogueTotal);
        EraseDialogue(); //erases showned dialogue
        printText(); //Prints new dialogue
        index++; //set it to 1
    }


    void printText()
    {
        //prints character name
        characterTextName.text = activeDialogue.dialogueConverstation[index].talkerName;
        this.activeDialogueText = activeDialogue.dialogueConverstation[index].text;

        //prints display text
        if (skipTextActive == true)
        {
            //show everything immediately
            displayText.text = this.activeDialogueText;
        }
        else
        {
            StartCoroutine(TypeWriting());
        }
        //saves the dialogue to dialogue log;
        //SaveDialogue();
    }
    //erase the dialogue
    void EraseDialogue()
    {
        //Clear all text
        displayText.text = "";
        characterTextName.text = "";
    }

    void replaceImage()
    {
        targetImage = activeDialogue.dialogueConverstation[index].characterImage;
    }

    //Shows a continue dialogue icon
    void showContIcon()
    {
        float arrowXPosition = boxWidth / 2.0f;
        float arrowYPosition = -boxHeight;
    }

    void ContCoverstation()
    {
        //erase the active dialogue
        EraseDialogue();

        //grayScale non talker
        //GrayScaleCharacters();

        //prints the text
        if (index < dialogueTotal)
            printText();

        index++;
    }

    void FinishConversation()
    {
        displayText.text = activeDialogue.dialogueConverstation[index].text;
    }
    //use if skiptextactive is set to false
    IEnumerator TypeWriting()
    {
        foreach (char letter in this.activeDialogueText.ToCharArray())
        {
            displayText.text += letter;
            yield return new WaitForSeconds(textSpeed);
        }
        if (index >= dialogueTotal)
        {

            isDialCompleted = true;
        }
    }

    public bool OnDialogueCompleted()
    {

        return isDialCompleted;
    }
}