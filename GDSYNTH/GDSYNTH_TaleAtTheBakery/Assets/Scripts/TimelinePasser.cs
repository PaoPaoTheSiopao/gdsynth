﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using UnityEngine.Events;

public class TimelinePasser : MonoBehaviour
{
    public PlayableDirector timelineDirector;
    public TimelineAsset timeline;

    public bool tlPlaying;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Robber")
        {
            if (timelineDirector.state != PlayState.Playing)
            {
                timelineDirector.Play(timeline);
            }
        }
    }

    public void SetActive()
    {
        this.gameObject.SetActive(true);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            //Debug.Log("Trigger = " + this.gameObject.name);

            //Debug.Log("Destroying : " + gameObject.name);
            Destroy(this.gameObject);
        }
    }

    public void PlayTLSignal(TimelineAsset tl)
    {
        timelineDirector.Play(tl);
        Debug.Log("Play : " + tl.name);
    }
}
