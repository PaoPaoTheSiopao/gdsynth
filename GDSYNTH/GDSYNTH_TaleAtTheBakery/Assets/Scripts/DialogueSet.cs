﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "DialogueSeries")]
public class DialogueSet : ScriptableObject
{
    public List<Dialogue> dialogueConverstation = new List<Dialogue>();
    private int currentItem;

    public void NextItem()
    {
        currentItem++;
    }
    public int CurItem()
    {
        return currentItem;
    }

    public int getTotalDialogue()
    {
        return dialogueConverstation.Count;
    }
}
