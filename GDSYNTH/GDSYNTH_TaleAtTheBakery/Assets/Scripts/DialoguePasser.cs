﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DialoguePasser : MonoBehaviour
{
    public DialogueSet dialSet;

    public UnityEvent OnFinishDialogue;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Return))
        {
            InstanceDial();
        }
    }

    public void InstanceDial()
    {
        DialogueSystem dial = DialogueSystem.instance;
        dial.gameObject.SetActive(true);
        Debug.Log("PassingDial = " + this.gameObject.name);
        dial.activateDialogue(dialSet);
        dial.OnDialogueComplete.AddListener(OnDialogueComplete);
    }

    public void OnDialogueComplete()
    {
        OnFinishDialogue.Invoke();
        DialogueSystem dial = DialogueSystem.instance;
        Disable();
        dial.OnDialogueComplete.RemoveListener(OnDialogueComplete);
    }

    public void Disable()
    {
        Debug.Log("Trigger = " + this.gameObject.name);
        DialogueSystem dial = DialogueSystem.instance;
        dial.gameObject.SetActive(false);
        this.gameObject.SetActive(false);
    }

    public void SignalToActivate(DialogueSet dialSetTL)
    {
        DialogueSystem dial = DialogueSystem.instance;
        dial.gameObject.SetActive(true);
        dial.activateDialogue(dialSetTL);
        dial.OnDialogueComplete.AddListener(OnDialogueComplete);
    }

}
