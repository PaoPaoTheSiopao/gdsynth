﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
[CreateAssetMenu(menuName = "Dialogue")]
public class Dialogue : DialogueSet
{

    //text
    public string talkerName;
    public string text;

    //character image
    //public GameObject characterImage;
    public Image characterImage;
    public Vector2 talkerBustPosition;
    public int objectIdIndex;
    public bool grayScaleNonTalker = true;
}


