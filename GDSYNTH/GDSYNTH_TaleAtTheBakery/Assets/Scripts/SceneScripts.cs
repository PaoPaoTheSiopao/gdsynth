﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneScripts : MonoBehaviour
{

    public void TheChoice(int i)
    {
        PlayersChoice.Instance.charChoice = i;
    }

	public void ChangeScene(int scence)
	{

		SceneManager.LoadScene(scence);
	}

	public void LowerAlphaImage(Image image)
	{
		image.color = new Color(1, 1, 1, 0.2f);
	}

	public void NormalAlphaImage(Image image)
	{
		image.color = new Color(1, 1, 1, 1f);
	}
     public void QuitButton()
    {
        Application.Quit();
    }


}
