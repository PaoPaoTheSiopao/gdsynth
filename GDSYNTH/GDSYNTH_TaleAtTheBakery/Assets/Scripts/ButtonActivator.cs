﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonActivator : MonoBehaviour
{
    public GameObject custButton;
    public GameObject cashButton;
    public GameObject bakeButton;
    // Start is called before the first frame update
    void Start()
    {
        if(PlayersChoice.Instance.charChoice == 1)
        {
            custButton.gameObject.SetActive(true);
            cashButton.gameObject.SetActive(false);
            bakeButton.gameObject.SetActive(false);
        }
        else if (PlayersChoice.Instance.charChoice == 2)
        {
            custButton.gameObject.SetActive(false);
            cashButton.gameObject.SetActive(true);
            bakeButton.gameObject.SetActive(false);
        }
        else if (PlayersChoice.Instance.charChoice == 3)
        {
            custButton.gameObject.SetActive(false);
            cashButton.gameObject.SetActive(false);
            bakeButton.gameObject.SetActive(true);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
