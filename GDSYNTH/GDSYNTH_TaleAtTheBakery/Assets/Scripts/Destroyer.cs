﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using UnityEngine.Events;
using UnityEngine.UI;
public class Destroyer : MonoBehaviour
{
    public float destroyIn;
    public PlayableDirector director;
    public TimelineAsset playAfterDestroy;
    public DialogueSet set;
    public GameObject extra;

    private Text txt;
    public UnityEvent OnFinishDialogue;
    public bool isDial = false;
    public bool shouldDestroy = false;
    private int instance = 0;
    // Start is called before the first frame update
    void Start()
    {
        txt = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        if(destroyIn<=0)
        {
            if (instance == 0)
            {
                if (!isDial)
                {
                    director.Play(playAfterDestroy);
                }
                else
                {
                    ActivateDial(set);
                }
                if(shouldDestroy)
                {
                    DestroyExtra();
                }
                txt.enabled = false;
                instance++;
            }
        }
        else
        {
            destroyIn -= Time.deltaTime;
        }
    }

    public void DestroyExtra()
    {
        Destroy(extra.gameObject);
    }

    public void ActivateDial(DialogueSet dialSetTL)
    {
        DialogueSystem dial = DialogueSystem.instance;
        dial.gameObject.SetActive(true);
        dial.activateDialogue(dialSetTL);
        dial.OnDialogueComplete.AddListener(OnDialogueComplete);
    }

    public void OnDialogueComplete()
    {
        OnFinishDialogue.Invoke();
        DialogueSystem dial = DialogueSystem.instance;
        Disable();
        dial.OnDialogueComplete.RemoveListener(OnDialogueComplete);
    }

    public void Disable()
    {
        Debug.Log("Trigger = " + this.gameObject.name);
        DialogueSystem dial = DialogueSystem.instance;
        dial.gameObject.SetActive(false);
        this.gameObject.SetActive(false);
    }
}
